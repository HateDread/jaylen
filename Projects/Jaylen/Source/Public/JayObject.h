#pragma once

#include "glm/vec3.hpp"
#include "glm/gtc/quaternion.hpp"
//
//namespace glm
//{
//	class dvec3;
//	class vec3;
//	class quat;
//}

/**
* Simple wrapper to data pointers.
*/
class JayObject
{
	friend class JayScene;
	friend class JayData;

public:
	unsigned int GetIndex() const { return m_index; }

	// Getters

	glm::dvec3 GetPosition() const;
	glm::vec3 GetVelocity() const;
	glm::vec3 GetForce() const;
	float GetMass() const;

	glm::quat GetOrientation() const;
	glm::vec3 GetAngularVelocity() const;
	glm::vec3 GetTorque() const;
	float GetMomentOfInertia() const;

	//glm::vec3 GetForwardVector() const;

	// Setters
	
	void AddForce(const glm::vec3& a_force);
	void AddTorque(const glm::vec3& a_torque);

	void AddRelativeForce(const glm::vec3& a_force);
	void AddRelativeTorque(const glm::vec3& a_torque);

	void SetVelocity(const glm::vec3& a_velocity);
	void SetRelativeVelocity(const glm::vec3& a_velocity);

	void SetPosition(const glm::dvec3& a_position);

	void SetOrientation(const glm::quat& a_orientation);
	void SetOrientation(const glm::vec3& a_euler);
	void SetOrientationRad(const glm::vec3& a_rads);
	void SetOrientation(float a_angle, const glm::vec3& a_axis);

	void SetMass(float a_mass);
	void SetMomentOfInertia(float a_momentOfInertia);

	// something about shape/dimensions/etc?

private:
	void SetIndex(unsigned int a_index) { m_index = a_index; }

	glm::dvec3* m_position;
	glm::vec3* m_velocity;
	glm::vec3* m_force;
	float* m_mass;

	glm::quat* m_orientation;
	glm::vec3* m_angularVelocity;
	glm::vec3* m_torque;
	float* m_momentOfInertia;

	unsigned int m_index;
};
