#pragma once

#include <vector>
#include "glm/vec3.hpp"
#include "glm/gtc/quaternion.hpp"

class JayObject;

class JayData
{
public:
	JayData(unsigned int a_maxObjects);

	void ConnectData(JayObject& a_object);

	std::vector<glm::dvec3>	m_positions;
	std::vector<glm::vec3>	m_velocities;
	std::vector<glm::vec3>	m_forces;
	std::vector<float>		m_masses;

	std::vector<glm::quat>	m_orientations;
	std::vector<glm::vec3>	m_angularVelocities;
	std::vector<glm::vec3>	m_torques;
	std::vector<float>		m_momentsofInertia;

private:
	const unsigned int m_maxObjects;

};