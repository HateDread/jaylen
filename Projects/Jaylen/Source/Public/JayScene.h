#pragma once

#include <vector>
#include <memory>
#include "JayObject.h"
#include "JayData.h"

class JayScene
{
public:
	JayScene(unsigned int a_maxObjects);
	~JayScene();

	JayObject* CreateObject();
	//void DestroyObject(JayObject* a_object);

	const std::vector<JayObject>& GetObjects() const;
	const JayData& GetData() const;

	void Step(double a_deltaTime);

	// Step simulation

	// AddForce
	/*
	void AddForce(const glm::vec3& a_force);
	void AddTorque(const glm::vec3& a_torque);

	void AddRelativeForce(const glm::vec3& a_force);
	void AddRelativeTorque(const glm::vec3& a_torque);

	void SetVelocity(const glm::vec3& a_velocity);
	void SetRelativeVelocity(const glm::vec3& a_velocity);

	glm::vec3 GetPosition() const;
	void SetPosition(const glm::vec3& a_position);

	glm::quat GetOrientation() const;
	void SetOrientation(const glm::quat& a_orientation);
	void SetOrientation(const glm::vec3& a_euler);
	void SetOrientationRad(const glm::vec3& a_euler);
	void SetOrientation(float a_angle, const glm::vec3& a_axis);

	glm::vec3 GetForwardVector() const;



	*/



private:
	void Integrate(double a_deltaTime);

	const unsigned int m_maxObjects;
	unsigned int m_nextObjectIdx = 0;
	std::vector<JayObject> m_objects;

	JayData m_data;

};