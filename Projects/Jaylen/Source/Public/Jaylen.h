#pragma once
#include <memory>

class JayScene;

class Jaylen
{
public:

	JayScene* CreateScene(unsigned int a_maxObjects);
	JayScene* GetScene();

private:
	std::unique_ptr<JayScene> m_scene;

};