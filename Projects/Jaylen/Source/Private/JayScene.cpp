#include "JayScene.h"

JayScene::~JayScene() = default;

JayScene::JayScene(unsigned int a_maxObjects)
	: m_maxObjects(a_maxObjects),
	m_data(a_maxObjects)
{
	m_objects.resize(m_maxObjects);

	// setup IDs
	for (unsigned int i = 0; i < m_objects.size(); ++i)
	{
		m_objects[i].SetIndex(i);
	}

	// Connect pointers
	for (unsigned int i = 0; i < m_objects.size(); ++i)
	{
		m_data.ConnectData(m_objects[i]);
	}
}

JayObject* JayScene::CreateObject()
{
	return &m_objects[m_nextObjectIdx++];
}

//void JayScene::DestroyObject(JayObject* a_object)
//{
//
//}

const std::vector<JayObject>& JayScene::GetObjects() const
{
	return m_objects;
}


const JayData& JayScene::GetData() const
{
	return m_data;
}

void JayScene::Step(double a_deltaTime)
{
	Integrate(a_deltaTime);
}

void JayScene::Integrate(double a_deltaTime)
{
	unsigned int numObjects = (unsigned int)m_data.m_positions.size();

	JayData& data = m_data;

	// linear
	for (unsigned int i = 0; i < m_maxObjects; ++i)
	{
		data.m_velocities[i] += ((data.m_forces[i] / data.m_masses[i]) * (float)a_deltaTime);
		data.m_positions[i] += glm::dvec3(data.m_velocities[i] * (float)a_deltaTime);
	}

	// reset force vectors
	memset(data.m_forces.data(), 0, sizeof(decltype(data.m_forces)::value_type) * data.m_forces.size());

	// rotation
	for (unsigned int i = 0; i < m_maxObjects; ++i)
	{
		data.m_angularVelocities[i] += ((data.m_torques[i] / data.m_momentsofInertia[i]) * (float)a_deltaTime);
		data.m_orientations[i] = glm::quat(data.m_angularVelocities[i]) * data.m_orientations[i];
	}

	// reset torque vectors
	memset(data.m_torques.data(), 0, sizeof(decltype(data.m_torques)::value_type) * data.m_torques.size());
}

