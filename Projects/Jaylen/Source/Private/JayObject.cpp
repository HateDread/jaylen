#include "JayObject.h"
#include "glm\gtc\quaternion.hpp"
#include <glm/gtx/quaternion.hpp>

glm::dvec3 JayObject::GetPosition() const
{
	return *m_position;
}

glm::vec3 JayObject::GetVelocity() const
{
	return *m_velocity;
}

glm::vec3 JayObject::GetForce() const
{
	return *m_force;
}

float JayObject::GetMass() const
{
	return *m_mass;
}

glm::quat JayObject::GetOrientation() const
{
	return *m_orientation;
}

glm::vec3 JayObject::GetAngularVelocity() const
{
	return *m_angularVelocity;
}

glm::vec3 JayObject::GetTorque() const
{
	return *m_torque;
}

float JayObject::GetMomentOfInertia() const
{
	return *m_momentOfInertia;
}

void JayObject::AddForce(const glm::vec3& a_force)
{
	*m_force += a_force;
}

void JayObject::AddTorque(const glm::vec3& a_torque)
{
	*m_torque += a_torque;
}

void JayObject::AddRelativeForce(const glm::vec3& a_force)
{
	glm::vec4 rotForce = glm::rotate(*m_orientation, glm::vec4(a_force, 1.0f));
	*m_force += rotForce.xyz();
}

void JayObject::AddRelativeTorque(const glm::vec3& a_torque)
{
	glm::vec4 rotTorque = glm::rotate(*m_orientation, glm::vec4(a_torque, 1.0f));
	*m_torque += rotTorque.xyz();
}

void JayObject::SetVelocity(const glm::vec3& a_velocity)
{
	*m_velocity = a_velocity;
}

void JayObject::SetRelativeVelocity(const glm::vec3& a_velocity)
{
	glm::vec4 rotVelocity = glm::rotate(*m_orientation, glm::vec4(a_velocity, 1.0f));
	*m_velocity = rotVelocity.xyz();
}

void JayObject::SetPosition(const glm::dvec3& a_position)
{
	*m_position = a_position;
}

void JayObject::SetOrientation(const glm::quat& a_orientation)
{
	*m_orientation = a_orientation;
}

void JayObject::SetOrientation(const glm::vec3& a_euler)
{
	glm::vec3 eulerRad = glm::radians(a_euler);
	*m_orientation = glm::quat(eulerRad);
}

void JayObject::SetOrientationRad(const glm::vec3& a_rads)
{
	*m_orientation = glm::quat(a_rads);
}

void JayObject::SetOrientation(float a_angle, const glm::vec3& a_axis)
{
	*m_orientation = glm::angleAxis(a_angle, a_axis);
}

void JayObject::SetMass(float a_mass)
{
	*m_mass = a_mass;
}

void JayObject::SetMomentOfInertia(float a_momentOfInertia)
{
	*m_momentOfInertia = a_momentOfInertia;
}
