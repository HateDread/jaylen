#include "Jaylen.h"
#include "JayScene.h"

JayScene* Jaylen::CreateScene(unsigned int a_maxObjects)
{
	m_scene = std::make_unique<JayScene>(a_maxObjects);
	return m_scene.get();
}

JayScene* Jaylen::GetScene()
{
	return m_scene.get();
}
