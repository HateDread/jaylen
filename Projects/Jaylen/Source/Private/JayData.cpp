#include "JayData.h"
#include "JayObject.h"

JayData::JayData(unsigned int a_maxObjects)
	: m_maxObjects(a_maxObjects)
{
	m_positions.resize(m_maxObjects);
	m_velocities.resize(m_maxObjects);
	m_forces.resize(m_maxObjects);
	m_masses.resize(m_maxObjects);

	m_orientations.resize(m_maxObjects);
	m_angularVelocities.resize(m_maxObjects);
	m_torques.resize(m_maxObjects);
	m_momentsofInertia.resize(m_maxObjects);
}

void JayData::ConnectData(JayObject& a_object)
{
	unsigned int idx = a_object.GetIndex();

	a_object.m_position = &m_positions[idx];
	a_object.m_velocity = &m_velocities[idx];
	a_object.m_force = &m_forces[idx];
	a_object.m_mass = &m_masses[idx];

	a_object.m_orientation = &m_orientations[idx];
	a_object.m_angularVelocity = &m_angularVelocities[idx];
	a_object.m_torque = &m_torques[idx];
	a_object.m_momentOfInertia = &m_momentsofInertia[idx];
}
